function f=diag_sum(A)
[m,n]=size(A);
a=min(m,n);
i=(a+1)/2;
A=A([i:a],[i:a]);
B=flip(A);
if mod(a,2)==0
    f=sum(diag(A))+sum(diag(B));
else
    f=sum(diag(A))+sum(diag(B))-A(i,i);
end
